#!/usr/bin/env sh
# -*- coding: utf-8 -*-
#this script is running inside docker image after docker run.
set -ex

apt update
apt upgrade -y
runuser -u www-data -- php occ config:system:set default_phone_region --type string --value="PL" #peek Your proper region like DE, GB, NL etc
echo "expose_php=Off" >> /usr/local/etc/php/conf.d/nextcloud.ini # disable x-powered-by header from apache server
sed -i "/opcache.interned_strings_buffer=/c\opcache.interned_strings_buffer=16" /usr/local/etc/php/conf.d/opcache-recommended.ini # resolve NC 23.0.2 OPcache warning
apt install --no-install-recommends -y libmagickcore-6.q16-6-extra # enable SVG support for image-magick
curl -sSLf \
        -o /usr/local/bin/install-php-extensions \
        https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions && \
        chmod +x /usr/local/bin/install-php-extensions && \
        install-php-extensions soap #install PHP-SOAP extention for TOTP plugin
