# Collabora admin panel
This panel can be accessed with credentials form docker-compose.yaml file 
https://docs.domain.com/browser/dist/admin/admin.html
```
            - username=admin #collabora admin panel user
            - password=test #collabora admin panel password
```

Some Hosting discovery (can be access with token)
https://docs.domain.com/hosting/discovery
