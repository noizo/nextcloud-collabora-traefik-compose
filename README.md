# Nextcloud + Collabora + Traefik + Cloudflare on Oracle Free Tier

This setup uses Cloudflare as main SSL provider for traefik.

This is a starting point for documenting full setup for Nexcloud instance on oracle free tier.
It WILL include:

1. ~Infra (Terraform)~
2. ~Instance provisioning (Ansible)~
3. *Docker compose for all services running on same machine.*
4. Dummy WP website under `domain.com`

# INFRA
A1 Flex instance + Loadbalancer (tbc) 
# ANSIBLE
tbd
# DOCKER
This repo held two docker-compose files.

## Traefik
`chmod 600 acme/acme.json`
`docker-compose up -d --force-recreate`

There are prerequuisites, You need to fulfill.

Obtain cloudflare credentials: (if You want to use cloudflare SSL certificates)
https://developers.cloudflare.com/api/tokens
```
CF_API_EMAIL=cloudflare.email@domain.com
CF_DNS_API_TOKEN=SOMESUPERSECRETAPITOKEN
CF_API_KEY=SOMESUPERSECRETAPIKEY
```

Put them in to file `.env` near `docker-compose.yml` file.

Chage Your domain names accordingly, and run:
`docker-compose up -d --force-recreate --remove-orphans`

Default login:password from `.htpasswd` is: `nextcloud:dashboard`

You can generate anything You like with:
`htpasswd -nb username password` (prerequisite: `apt install apache2-utils`)

## Nextcloud 

The only prerequisite for nextcloud is running traefik.
`docker-compose up -d --force-recreate --remove-orphans`

### Updating Nextcloud
After new netcloud release is there, run `docker-compose pull` & `docker-compose up -d --force-recreate --remove-orphans`. It will restart nextcloud and all other services with latest image.

After that You will see a warning in Overview, telling You that not all db indicies are in place.
Run this manually on Your host. `docker exec -u 33 -it nextcloud-app ./occ db:add-missing-indices`


## Dummy WP
It's just an example of how fast and robust this setup is.
Just launch new docker-compose with labels, and You're up!


tbc
